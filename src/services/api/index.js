const API = process.env.NEXT_PUBLIC_API_URL;
//const VERSION = process.env.NEXT_PUBLIC_API_VERSION;

const endPoints = {
  balance: {
    getById: (id) => `${API}/balance/${id}/`,
    create: `${API}/balance/`,
    getAll: (limit, page) => `${API}/balance?limit=${limit}&page=${page}`,
  }
};

export default endPoints;
